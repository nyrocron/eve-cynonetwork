#!/usr/bin/env python3

from os import path
from evestatic import StaticDb
from PIL import Image, ImageDraw, ImageFont

import config

shiptypes = {
    'dread': 5,
    'carrier': 6.5,
    'jf': 5,
}

img_width, img_height = config.img_size
coords_to_img = lambda x, y: (int(x * (img_width - 1)), int((img_width - 1) - ((img_width - 1) * y)))

db = StaticDb()


def make_map_path(map_name, thumb=False):
    filename = '{}{}.png'.format(map_name, '_th' if thumb else '')
    return path.join(config.out_dir, filename)

def makemap(network_ids, shiptype, jdc, exclude_nullsec=False, map_filename=None, create_thumbnail=True):
    maxrange = shiptypes[shiptype] * (1 + 0.25 * jdc)

    in_range, out_of_range = db.get_network_range(network_ids, maxrange, exclude_nullsec=exclude_nullsec)

    coords = db.get_normalized_coords(exclude_nullsec=exclude_nullsec)

    img = Image.new('RGB', (img_width, img_height))
    draw = ImageDraw.Draw(img)

    label_font = ImageFont.truetype(config.label_font, 12)

    color_hisec = (0, 128, 255)
    color_inrange = (0, 255, 0)
    color_outofrange = (255, 0, 0)
    color_network = (255, 255, 0)

    for sysid in db.get_systems(exclude_nullsec=True, exclude_lowsec=True):
        x, y, z = coords[sysid]
        img.putpixel(coords_to_img(x, z), color_hisec)

    for sysid in out_of_range:
        x, y, z = coords[sysid]
        img.putpixel(coords_to_img(x, z), color_outofrange)

    for sysid in in_range:
        x, y, z = coords[sysid]
        img.putpixel(coords_to_img(x, z), color_inrange)

    for sysid in network_ids:
        x, y, z = coords[sysid]

        img_coords = coords_to_img(x, z)
        img_x, img_y = img_coords

        draw.polygon([(img_x - 2, img_y), (img_x, img_y - 2), (img_x + 2, img_y), (img_x, img_y + 2)], fill=color_network)

        label_text = db.get_sys_name(sysid)
        if sysid in out_of_range:
             label_text += " (not reachable)"

        if not db.get_sys_has_med(sysid):
            label_text += " ({}j to cloning)".format(db.get_sys_med_dist(sysid))

        label_size = draw.textsize(label_text, label_font)
        if img_x + label_size[0] + 3 > img_width:
            label_coords = (img_x - label_size[0] - 3, img_y + 3)
        else:
            label_coords = img_x + 3, img_y + 3
        draw.text(label_coords, label_text, font=label_font)

    # info text
    draw.text((5, 5), "JDC {} {}".format(jdc, shiptype), font=label_font)
    draw.text((5, 20), "Lowsec only" if exclude_nullsec else "Including 0.0", font=label_font)
    draw.text((5, 35), "In range: {}".format(len(in_range)), font=label_font)
    draw.text((5, 50), "Not in range: {}".format(len(out_of_range)), font=label_font)

    # legend
    draw.rectangle((5, 77, 13, 85), fill=color_hisec)
    draw.text((18, 75), "Highsec", font=label_font)
    draw.rectangle((5, 92, 13, 100), fill=color_inrange)
    draw.text((18, 90), "In range", font=label_font)
    draw.rectangle((5, 107, 13, 115), fill=color_outofrange)
    draw.text((18, 105), "Out of range", font=label_font)
    img_x, img_y = 9, 127
    draw.polygon([(img_x - 2, img_y), (img_x, img_y - 2), (img_x + 2, img_y), (img_x, img_y + 2)], fill=color_network)
    draw.text((18, 120), "Network System", font=label_font)

    if map_filename is None:
        map_name = "map_{}_{}_{}".format("LS" if exclude_nullsec else "NS", shiptype, jdc)
    else:
        map_name = map_filename

    img.save(make_map_path(map_name))

    if create_thumbnail:
        img.thumbnail(config.thumb_size, resample=Image.ANTIALIAS)
        img.save(make_map_path(map_name, True))


if __name__ == '__main__':
    network_systems = [line.strip().split()[0] for line in open(config.network_file) if len(line.strip()) > 0]
    network_ids = [db.get_sys_id(name) for name in network_systems]
    for shiptype, jdc, sec in config.wanted_maps:
        ls_only = sec == 'low'
        makemap(network_ids, shiptype, jdc, exclude_nullsec=ls_only)
