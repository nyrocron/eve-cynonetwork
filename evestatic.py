import sqlite3
from math import sqrt
from copy import copy


class InvalidSystemError(ValueError):
    def __init__(self, message, *args, **kwargs):
        ValueError.__init__(self, "Invalid system name: {}".format(message), *args, **kwargs)


class StaticDb(object):
    def __init__(self):
        self.db_connection = sqlite3.connect('static.db')
        self.db_cursor = self.db_connection.cursor()

    def get_typename(self, type_id):
        self.db_cursor.execute('SELECT typeName FROM invTypes WHERE typeID = ?', (type_id,))
        db_result = self.db_cursor.fetchone()
        if db_result is None:
            return None
        return db_result[0]

    def get_sys_id(self, system_name):
        self.db_cursor.execute('SELECT solarSystemID FROM mapSolarSystems WHERE solarSystemName = ?', (system_name,))
        db_result = self.db_cursor.fetchone()

        if db_result is None:
            raise InvalidSystemError(system_name)

        return db_result[0]

    def get_sys_name(self, system_id):
        self.db_cursor.execute('SELECT solarSystemName FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchone()
        return None if db_result is None else db_result[0]

    def get_sys_sec(self, system_id):
        self.db_cursor.execute('SELECT security FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchone()
        return None if db_result is None else db_result[0]

    def get_sys_coords(self, system_id):
        self.db_cursor.execute('SELECT x, y, z FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchone()
        return None if db_result is None else db_result

    def get_sys_region(self, system_id):
        self.db_cursor.execute('SELECT regionID FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchone()
        return None if db_result is None else db_result[0]

    def get_sys_constellation(self, system_id):
        self.db_cursor.execute('SELECT constellationID FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchone()
        return None if db_result is None else db_result[0]

    def get_sys_kspace(self, system_id, sec=None):
        if sec is None:
            sec = self.get_sys_sec(system_id)

        if sec > -0.99:
            return True

        self.db_cursor.execute('SELECT wc.wormholeclassID FROM mapLocationWormholeClasses wc JOIN mapSolarSystems s'
                               ' ON wc.locationID = s.solarSystemID'
                               '  OR wc.locationID = s.constellationID'
                               '  OR wc.locationID = s.regionID'
                               ' WHERE s.solarSystemID = ?',
                               (system_id,))
        result = self.db_cursor.fetchone()

        if result is not None:
            return result[0] > 6

        return False # jove space

    def get_sys_adjacent(self, system_id):
        self.db_cursor.execute('SELECT toSolarSystemID FROM mapSolarSystemJumps WHERE fromSolarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchall()
        if db_result is None:
            return []
        return [row[0] for row in db_result]

    def get_sys_medstations(self, system_id):
        self.db_cursor.execute('SELECT st.stationID'
                               ' FROM staStations st JOIN staOperationServices os ON st.operationID = os.operationID'
                               ' WHERE os.serviceID = 512 AND st.solarSystemID = ?', (system_id,))
        db_result = self.db_cursor.fetchall()
        if db_result is None:
            return []

        return [row[0] for row in db_result]

    def get_sys_has_med(self, system_id):
        return len(self.get_sys_medstations(system_id)) > 0

    def get_sys_med_dist(self, system_id):
        to_explore = [(system_id, 0)]
        while len(to_explore) > 0:
            current, dist = to_explore.pop(0)
            if self.get_sys_has_med(current):
                return dist
            for sysid in self.get_sys_adjacent(current):
                to_explore.append((sysid, dist + 1))

        raise Exception("No medical facilities found off system " + str(system_id))

    def get_distance(self, system_a, system_b):
        if system_a == system_b:
            return 0
        self.db_cursor.execute('SELECT x, y, z FROM mapSolarSystems WHERE solarSystemID = ? OR solarSystemID = ?',
                               (system_a, system_b))
        loc_a = self.db_cursor.fetchone()
        if loc_a is None:
            raise ValueError('One of these system IDs is invalid: ' + str(system_a) + ', ' + str(system_b))
        loc_b = self.db_cursor.fetchone()
        if loc_b is None:
            raise ValueError('One of these system IDs is invalid: ' + str(system_a) + ', ' + str(system_b))

        x_a, y_a, z_a = loc_a
        x_b, y_b, z_b = loc_b
        return sqrt((x_a - x_b)**2 + (y_a - y_b)**2 + (z_a - z_b)**2) / 9460730472580800 # m -> ly

    def get_systems(self, kspace_only=False, exclude_hisec=False, exclude_nullsec=False, exclude_lowsec=False):
        where_clause = []
        if exclude_hisec:
            where_clause.append('security < 0.5')
        if exclude_nullsec:
            where_clause.append('security > 0')
        if exclude_lowsec:
            where_clause.append('(security <= 0 OR security >= 0.5)')

        if len(where_clause) > 0:
            self.db_cursor.execute('SELECT solarSystemID FROM mapSolarSystems WHERE ' + ' AND '.join(where_clause))
        else:
            self.db_cursor.execute('SELECT solarSystemID FROM mapSolarSystems')

        rows = self.db_cursor.fetchall()
        if rows is None:
            raise Exception('failed to query systems')

        systems = [row[0] for row in rows]
        del rows

        if kspace_only:
            return [sysid for sysid in systems if self.get_sys_kspace(sysid)]

        return systems

    def get_systems_in_range(self, system_id, max_distance, exclude_hisec=False, exclude_nullsec=False):
        self.db_cursor.execute('SELECT x, y, z FROM mapSolarSystems WHERE solarSystemID = ?', (system_id,))
        src_coords = self.db_cursor.fetchone()
        if src_coords is None:
            raise ValueError('invalid system id')

        max_dist_m = StaticDb.ly_to_m(max_distance)
        x, y, z = src_coords

        where_clause = [
            'x >= ?', 'x <= ?',
            'y >= ?', 'y <= ?',
            'z >= ?', 'z <= ?',
        ]

        if exclude_hisec:
            where_clause.append('security < 0.5')
        if exclude_nullsec:
            where_clause.append('security > 0')

        self.db_cursor.execute('SELECT solarSystemID, x, y, z FROM mapSolarSystems WHERE ' + ' AND '.join(where_clause),
                               (x - max_dist_m, x + max_dist_m,
                                y - max_dist_m, y + max_dist_m,
                                z - max_dist_m, z + max_dist_m))
        in_range = []
        for dst in self.db_cursor.fetchall():
            dst_id = dst[0]
            dst_coords = dst[1:]
            if dst_id != system_id and StaticDb.ly_distance(src_coords, dst_coords) <= max_distance:
                in_range.append(dst_id)

        return in_range

    def get_network_range(self, network_systems, max_distance, exclude_nullsec=False):
        network_systems = copy(network_systems)

        if exclude_nullsec:
            self.db_cursor.execute('SELECT solarSystemID, security, x, y, z FROM mapSolarSystems WHERE security < 0.5 AND security > 0')
            all_systems = self.db_cursor.fetchall()
            if all_systems is None:
                raise Exception('system query failed')

        else:
            self.db_cursor.execute('SELECT solarSystemID, security, x, y, z FROM mapSolarSystems WHERE security < 0.5')
            db_result = self.db_cursor.fetchall()
            if db_result is None:
                raise Exception('system query failed')
            all_systems = [sys for sys in db_result if self.get_sys_kspace(sys[0], sys[1])] # filter out wspace systems

        coords = dict()
        to_check = []
        for sysid, sec, x, y, z in all_systems:
            coords[sysid] = (x, y, z)
            to_check.append(sysid)

        network_inrange = set()
        network_inrange.add(network_systems.pop(0))
        in_range = []

        max_dist_m = StaticDb.ly_to_m(max_distance)

        while len(network_inrange) > 0:
            current = network_inrange.pop()
            c_x, c_y, c_z = coords[current]
            in_range.append(current)

            out_of_range = []
            for sysid in to_check:
                x, y, z = coords[sysid]
                if sqrt((c_x - x)**2 + (c_y - y)**2 + (c_z - z)**2) < max_dist_m:
                    if sysid in network_systems:
                        network_systems.remove(sysid)
                        network_inrange.add(sysid)

                    else:
                        in_range.append(sysid)

                else:
                    out_of_range.append(sysid)

            to_check = out_of_range

        return in_range, to_check

    def get_normalized_coords(self, exclude_nullsec=False):
        if exclude_nullsec:
            self.db_cursor.execute('SELECT solarSystemID, x, y, z FROM mapSolarSystems WHERE security > 0')
            systems = self.db_cursor.fetchall()
            if systems is None:
                raise Exception('system query failed')
        else:
            self.db_cursor.execute('SELECT solarSystemID, x, y, z, security FROM mapSolarSystems')
            db_result = self.db_cursor.fetchall()
            if db_result is None:
                raise Exception('system query failed')

            systems = [(sysid, x, y, z) for sysid, x, y, z, sec in db_result if self.get_sys_kspace(sysid, sec)]

        x_min, x_max = 0, 0
        y_min, y_max = 0, 0
        z_min, z_max = 0, 0

        for sysid, x, y, z in systems:
            x_min = min(x, x_min)
            x_max = max(x, x_max)
            y_min = min(y, y_min)
            y_max = max(y, y_max)
            z_min = min(z, z_min)
            z_max = max(z, z_max)

        x_delta = x_max - x_min
        y_delta = y_max - y_min
        z_delta = z_max - z_min
        delta = max(x_delta, y_delta, z_delta)

        normalized_coords = dict()
        for sysid, x, y, z in systems:
            norm_x = (x - x_min + (delta - x_delta) / 2) / delta
            norm_y = (y - y_min + (delta - y_delta) / 2) / delta
            norm_z = (z - z_min + (delta - z_delta) / 2) / delta
            normalized_coords[sysid] = (norm_x, norm_y, norm_z)

        return normalized_coords

    @staticmethod
    def ly_distance(coords_a, coords_b):
        return StaticDb.m_to_ly(sqrt(sum([(x-y)**2 for x,y in zip(coords_a, coords_b)])))

    @staticmethod
    def m_to_ly(x):
        return x / 9460730472580800

    @staticmethod
    def ly_to_m(x):
        return x * 9460730472580800
