#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import parse_qs, urljoin
from os import urandom
from os.path import isfile
from datetime import datetime

import server_config as config

from cynorange import makemap, shiptypes, make_map_path 
from evestatic import StaticDb, InvalidSystemError

db = StaticDb()


def require_headers_or_400(required):
    def decorate(f):
        def decorated(self, *args, **kwargs):
            for header in required:
                if header not in self.headers:
                    self.send_error(400, "Header missing: {}".format(header))
                    return None

            return f(self, *args, **kwargs)
        return decorated
    return decorate

def form_response_or_400(f):
    @require_headers_or_400(['Content-Type', 'Content-Length'])
    def decorated(self, *args, **kwargs):
        if self.headers['Content-Type'] != 'application/x-www-form-urlencoded':
            self.send_error(400, "Expected form response")
            return None

        return f(self, *args, **kwargs)
    return decorated

def is_character(c):
    return 97 <= int(c) <= 122

def random_string(length=32):
    string = ''
    while len(string) < length:
        random_buffer = urandom(256)
        string += bytes([c for c in random_buffer if is_character(c)]).decode('ascii')
    return string[:length]

def log(log_path, message):
    with open(log_path, 'a') as logfile:
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        logfile.write("[{}] {}\n".format(timestamp, message))


class MapRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        form_data = self.get_form_response()
        if form_data is None:
            return

        try:
            network = [db.get_sys_id(sysname) for sysname in form_data['network'].splitlines()]
        except InvalidSystemError as e:
            self.log_send_error(str(e), error_code=400)
            return

        network = [sysid for sysid in network if db.get_sys_sec(sysid) < 0.5]
        
        shiptype = form_data['shiptype']
        if shiptype not in shiptypes:
            self.log_send_error("invalid shiptype: {}".format(shiptype), error_code=400)
            return

        jdc = int(form_data['jdc'])
        if jdc not in range(3, 6):
            self.log_send_error("invalid level of JDC: {}".format(jdc), error_code=400)
            return

        lowsec = form_data['lowsec'] == 'yes'

        namelen = 4
        while True:
            mapname = random_string(namelen)
            if isfile(make_map_path(mapname)):
                namelen += 1
            else:
                break
        
        try:
            makemap(network, shiptype, jdc, exclude_nullsec=lowsec,
                    map_filename=mapname, create_thumbnail=False)
        except Exception as e:
            self.log_send_error("error while creating map",
                                extended_message="makemap error {} DATA {}".format(e, network))
            return

        image_url = urljoin(config.image_baseurl, mapname + '.png')

        log(config.access_log, "created map {}".format(mapname))

        self.redirect(image_url)

    @form_response_or_400
    def get_form_response(self):
        data_encoding = self.headers['Content-Encoding']
        if data_encoding is None:
            data_encoding = 'utf-8'

        form_data_urlencoded = self.rfile.read(int(self.headers['Content-Length'])).decode(data_encoding)
        form_data = parse_qs(form_data_urlencoded)

        for k in form_data:
            form_data[k] = ','.join(form_data[k])

        return form_data

    def redirect(self, redirect_url):
        self.send_response(302)
        self.send_header('Location', redirect_url)
        self.end_headers()

    def log_send_error(self, message, error_code=500, extended_message=None):
        log(config.error_log, "{} {}".format(error_code, message if extended_message is None else extended_message))
        self.send_error(error_code, message)

if __name__ == '__main__':
    server = HTTPServer(('', config.port), MapRequestHandler)
    server.serve_forever()
